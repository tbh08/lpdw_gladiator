using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LpdwGladiator
{
    public class Shop
    {
        List<Item> ItemList = new List<Item>();

        public Shop(){
            SetItems();
        }

        public void SetItems()
        {
            ItemList.Add(new Item("Dague"  , 2, 0.60,    0, 1, false));
            ItemList.Add(new Item("Casque" , 2,    0, 0.10, 0, false));
            ItemList.Add(new Item("Lance"  , 7, 0.50,    0, 4, false));
            ItemList.Add(new Item("Filet"  , 3, 0.30,    0, 5, true ));
            ItemList.Add(new Item("�p�e"   , 5, 0.70,    0, 2, false));
            ItemList.Add(new Item("Trident", 7, 0.40, 0.10, 3, false));
            ItemList.Add(new Item("Bouclier rectangulaire", 8, 0, 0.30, 0, false));
            ItemList.Add(new Item("Petit bouclier rond"   , 5, 0, 0.20, 0, false));
        }

        public void GetItems()
        {
            if (ItemList.Count != 0) { 
                Console.WriteLine();
                Console.WriteLine("Voici les armes et accessoires disponibles dans la boutique :");
                Console.WriteLine("==================================");
                foreach (var item in ItemList)
                {
                    Console.WriteLine(" " + item.Name + " :");
                    Console.WriteLine("  - Cout : " + item.Cost + " pts");
                    Console.WriteLine("  - Attaque : " + item.Attack + " %");
                    Console.WriteLine("  - Parade : " + item.Parry + " %");
                    Console.WriteLine("  - Usage Unique : " + item.HasOneTimeUsage);
                    Console.WriteLine("==================================");
                }    
                Console.WriteLine();
            } else
            {
                Console.WriteLine();
                Console.WriteLine("Hum... Il y a comme un ptit probl�me : la boutique est vide !");
                Console.WriteLine();
            }
        }

        public List<Item> GetItemList()
        {
            return ItemList;
        }

        public void ShowThisItemFromList(string item)
        {
            var thisItem = ItemList.FirstOrDefault(x => x.Name == item);

            if (thisItem != null)
            {
                Console.WriteLine();
                Console.WriteLine("Voici les caract�ristiques de \"" + item + "\" :");
                Console.WriteLine("==================================");
                Console.WriteLine(" " + thisItem.Name + " :");
                Console.WriteLine("  - Cout : " + thisItem.Cost + " pts");
                Console.WriteLine("  - Attaque : " + thisItem.Attack + " %");
                Console.WriteLine("  - Parade : " + thisItem.Parry + " %");
                Console.WriteLine("  - Usage Unique : " + thisItem.HasOneTimeUsage);
                Console.WriteLine("==================================");
                Console.WriteLine();
            } else
            {
                Console.WriteLine();
                Console.WriteLine(" \"" + item + "\" introuvable.");
                Console.WriteLine("V�rifiez s'il existe dans la boutique et v�rifiez �galement l'orteaugraff.");
                Console.WriteLine();
            }
        }

        public void ShowThisItemFromList(int item)
        {
            var thisItem = ItemList[item];

            if (thisItem != null)
            {
                Console.WriteLine();
                Console.WriteLine("Voici les caract�ristiques de \"" + thisItem.Name + "\" (" + item + "):");
                Console.WriteLine("==================================");
                Console.WriteLine(" " + thisItem.Name + " :");
                Console.WriteLine("  - Cout : " + thisItem.Cost + " pts");
                Console.WriteLine("  - Attaque : " + thisItem.Attack + " %");
                Console.WriteLine("  - Parade : " + thisItem.Parry + " %");
                Console.WriteLine("  - Usage Unique : " + thisItem.HasOneTimeUsage);
                Console.WriteLine("==================================");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine(" \"" + item + "\" introuvable.");
                Console.WriteLine("V�rifiez s'il existe dans la boutique et v�rifiez �galement l'orteaugraff.");
                Console.WriteLine();
            }
        }

        public Item GetThisItemFromList(int item)
        {
            var thisItem = ItemList[item];
            return thisItem;
        }

        public Item GetThisItemFromList(string item)
        {
            var thisItem = ItemList.FirstOrDefault(x => x.Name == item);
            return thisItem;
        }

        /*        
        public List<Item> GetThisItemFromList(string item)
        {
            List<Item> thisItem = ItemList.Where(x => x.Name == item).ToList();
            return thisItem;
        }

        public void ShowThisItemFromList(string item)
        {
            List<Item> thisItem = ItemList.Where(x => x.Name == item).ToList();
            foreach (var oneitem in thisItem)
            {
                Console.WriteLine("Item: {0},{1},{2},{3},{4}", oneitem.Name, oneitem.Cost, oneitem.Attack, oneitem.Parry, oneitem.HasOneTimeUsage);
            }
        }*/
    }
}
