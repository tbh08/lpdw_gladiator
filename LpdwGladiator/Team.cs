using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LpdwGladiator
{
    public class Team
    {
        private string name;
        private string desc;
        private int matchPlayed;
        private int matchWon;
        private double matchRatio;
        private int maxGladiators = 3;
        private int leftGladiators = 3;
        List<Gladiator> TeamGladiators = new List<Gladiator>();
        public List<Gladiator> TeamGladiatorsSorted = new List<Gladiator>();

        public Team (string name, string desc)
        {
            this.name = name;
            this.desc = desc;
        }

        public void AddGladiator(Gladiator gladiator)
        {
            TeamGladiators.Add(gladiator);
            Console.WriteLine("Gladiateur ajout� !");
            leftGladiators--;
            Console.WriteLine(" " + leftGladiators + " places restantes dans votre �quipre.");
        }

        public List<Gladiator> GetTeamGladiators()
        {
            return TeamGladiators;
        }

        public void ShowTeamGladiators()
        {
            if (TeamGladiators.Count != 0)
            {
                Console.WriteLine();
                Console.WriteLine("Voici la liste des gladiateurs de : " + Name);
                Console.WriteLine("==================================");
                foreach (var gladiator in TeamGladiators)
                {
                    Console.WriteLine(" " + gladiator.Name + " :");
                    Console.WriteLine("  - Priorit� : " + gladiator.Priority);
                    Console.WriteLine("  - Victoires : //TODO");
                    Console.WriteLine("  - D�faites : //TODO");
                    Console.WriteLine("  - Ratio : //TODO %");
                    Console.WriteLine("  ? Liste des �quipements ? : //TODO");
                    Console.WriteLine("==================================");
                }
                Console.WriteLine("> Places restantes dans l'�quipe : " + leftGladiators);
                Console.WriteLine();
            }
        }

        public void SetGladiatorsPriority(Gladiator gladiator1 = null, Gladiator gladiator2 = null, Gladiator gladiator3 = null)
        {
            if (gladiator1 != null) {
                gladiator1.Priority = 1;
            };
            if (gladiator2 != null) {
                gladiator2.Priority = 2;
            };
            if (gladiator3 != null) {
                gladiator3.Priority = 3;
            };
            List<Gladiator> TeamGladiatorsSorted = TeamGladiators.OrderBy(o => o.Priority).ToList();
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Desc
        {
            get
            {
                return desc;
            }

            set
            {
                desc = value;
            }
        }

        public int MatchPlayed
        {
            get
            {
                return matchPlayed;
            }

            set
            {
                matchPlayed = value;
            }
        }

        public int MatchWon
        {
            get
            {
                return matchWon;
            }

            set
            {
                matchWon = value;
            }
        }

        public double MatchRatio
        {
            get
            {
                return matchRatio;
            }

            set
            {
                matchRatio = value;
            }
        }

        public int MaxGladiators
        {
            get
            {
                return maxGladiators;
            }

            set
            {
                maxGladiators = value;
            }
        }

        public int LeftGladiators
        {
            get
            {
                return leftGladiators;
            }

            set
            {
                leftGladiators = value;
            }
        }

        public List<Gladiator> TeamGladiatorsSorted1
        {
            get
            {
                return TeamGladiatorsSorted;
            }

            set
            {
                TeamGladiatorsSorted = value;
            }
        }
    }
}
