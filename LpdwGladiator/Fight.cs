using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LpdwGladiator
{
    public class Fight
    {
        private int attacker;
        private Item weaponG1;
        private Item weaponG2;
        private Gladiator gladiator1;
        private Gladiator gladiator2;
        private Dice diceOfHundred = new Dice(1, 100);

        public Fight(int attacker, Item weaponG1, Item weaponG2, Gladiator gladiator1, Gladiator gladiator2)
        {
            this.attacker = attacker;
            this.weaponG1 = weaponG1;
            this.weaponG2 = weaponG2;
            this.gladiator1 = gladiator1;
            this.gladiator2 = gladiator2;

            if (attacker == 1)
            {
                Console.WriteLine(Attack(weaponG1, 2));
            } else
            {
                Console.WriteLine(Attack(weaponG2, 1));
            }

            List<Item> Equipementg1 = gladiator1.GetEquipment();
            List<Item> Equipementg2 = gladiator2.GetEquipment();

            //phase 1 : priorit� des armes
            List<Item> Equipementg1Sorted = Equipementg1.OrderByDescending(o => o.Priority).ToList();
            List<Item> Equipementg2Sorted = Equipementg2.OrderByDescending(o => o.Priority).ToList();

            var imaxG1 = Equipementg1Sorted.Count();
            var imaxG2 = Equipementg2Sorted.Count();
        }

        public string Attack(Item weapon, int ennemy)
        {
            
            int randomNumber = diceOfHundred.DiceRoll();
            
            if (randomNumber/100 < weapon.Attack)
            {
                
                return "Le coup porte";
            } else {
                return "Attaque rat�e";
            }
        }

        public bool Parry(Item item)
        {
            int randomNumber = diceOfHundred.DiceRoll();

            if (randomNumber / 100 < item.Parry)
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
