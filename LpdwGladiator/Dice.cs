﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LpdwGladiator
{
    class Dice
    {
        private Random rnd = new Random();
        private int min;
        private int max;

        public Dice(int min, int max)
        {
            this.min = min;
            this.max = max + 1;
            DiceRoll();
        }

        public int DiceRoll()
        {   
            int diceRoll = rnd.Next(min, max); //Roll the Dice !
            return diceRoll;
        }
    }
}
