using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LpdwGladiator
{
    public class Player
    {
        private string lastName;
        private string firstName;
        private string nickName;
        private DateTime subscribeDate;
        private int maxTeams = 5;
        private int leftTeams = 5;
        List<Team> playerTeams = new List<Team>();
        List<Team> playerTeamsSorted = new List<Team>();

        public Player (string lastName, string firstName, string nickName)
        {
            this.lastName = lastName;
            this.firstName = firstName;
            this.nickName = nickName;
            subscribeDate = DateTime.Now;
        }

        public void AddTeam(Team team)
        {
            playerTeams.Add(team);
            Console.WriteLine("�quipe ajout�e !");
            leftTeams--;
            Console.WriteLine(" Vous pouvez encore ajouter " + leftTeams + " autres �quipes.");
        }

        public List<Team> GetPlayerTeams()
        {
            List<Team> playerTeamsSorted = playerTeams.OrderByDescending(o => o.MatchRatio).ToList();
            return playerTeamsSorted;
        }

        public void ShowPlayerTeams()
        {
            List<Team> playerTeamsSorted = playerTeams.OrderByDescending(o => o.MatchRatio).ToList();
            if (playerTeamsSorted.Count != 0)
            {
                Console.WriteLine();
                Console.WriteLine("Voici la liste des �quipes de : " + firstName + " " + lastName);
                Console.WriteLine("==================================");
                foreach (var team in playerTeamsSorted)
                {
                    Console.WriteLine(" " + team.Name + " :");
                    Console.WriteLine("  - Description : " + team.Desc);
                    Console.WriteLine("  - Victoires : //TODO");
                    Console.WriteLine("  - D�faites : //TODO");
                    Console.WriteLine("  - Ratio : //TODO %");
                    Console.WriteLine("  ? Liste des gladiateurs dans la team ? : //TODO");
                    Console.WriteLine("==================================");
                }
                Console.WriteLine("> Il reste " + leftTeams + " emplacements libres d'�quipe");
                Console.WriteLine();
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }

            set
            {
                lastName = value;
            }
        }

        public string FirstName
        {
            get
            {
                return firstName;
            }

            set
            {
                firstName = value;
            }
        }

        public string NickName
        {
            get
            {
                return nickName;
            }

            set
            {
                nickName = value;
            }
        }

        public DateTime SubscribeDate
        {
            get
            {
                return subscribeDate;
            }
        }

        public int MaxTeams
        {
            get
            {
                return maxTeams;
            }

            set
            {
                maxTeams = value;
            }
        }

        public int LeftTeams
        {
            get
            {
                return leftTeams;
            }

            set
            {
                leftTeams = value;
            }
        }
       
    }
}
