using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LpdwGladiator
{
    public class Gladiator
    {
        string name;
        private int maxPoints = 10;
        private int leftPoints = 10;
        Shop shop = new Shop();
        List<Item> Equipment = new List<Item>();
        private int priority;

        public Gladiator(string name)
        {
            this.name = name;
        }

        public void ChooseNewItem(string item)
        {
            Item ItemChosen = shop.GetThisItemFromList(item);
            if (ItemChosen != null)
            {
                if (ItemChosen.Cost <= LeftPoints)
                {
                    Equipment.Add(ItemChosen);
                    leftPoints = leftPoints - ItemChosen.Cost;
                    Console.WriteLine();
                    Console.WriteLine(ItemChosen.Name + " ajout� � l'�quipement de " + Name);
                    Console.WriteLine("> Points restants : " + LeftPoints);
                    Console.WriteLine();
                } else
                {
                    Console.WriteLine();
                    Console.WriteLine("Erreur : pas assez de points disponibles.");
                    Console.WriteLine("Vos " + LeftPoints + " restants ne sont pas suffisants");
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Erreur \"" + item + "\" non ajout�.");
                Console.WriteLine("> Points restants : " + LeftPoints);
                Console.WriteLine();
            }
        }

        public void ChooseNewItem(int item)
        {
            Item ItemChosen = shop.GetThisItemFromList(item);
            if (ItemChosen != null)
            {
                if (ItemChosen.Cost <= LeftPoints)
                {
                    Equipment.Add(ItemChosen);
                    leftPoints = leftPoints - ItemChosen.Cost;
                    Console.WriteLine();
                    Console.WriteLine(ItemChosen.Name + " ajout� � l'�quipement de " + Name);
                    Console.WriteLine("> Points restants : " + LeftPoints);
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Erreur : pas assez de points disponibles.");
                    Console.WriteLine("Vos " + LeftPoints + " restants ne sont pas suffisants");
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Erreur \"" + item + "\" non ajout�.");
                Console.WriteLine("> Points restants : " + LeftPoints);
                Console.WriteLine();
            }
        }

        public List<Item> GetEquipment()
        {
            return Equipment;
        }

        public void ShowEquipement()
        {
            if (Equipment.Count != 0)
            {
                Console.WriteLine();
                Console.WriteLine("Voici l'�quipement de :" + Name);
                Console.WriteLine("==================================");
                foreach (var item in Equipment)
                {
                    Console.WriteLine(" " + item.Name + " :");
                    Console.WriteLine("  - Cout : " + item.Cost + " pts");
                    Console.WriteLine("  - Attaque : " + item.Attack + " %");
                    Console.WriteLine("  - Parade : " + item.Parry + " %");
                    Console.WriteLine("  - Usage Unique : " + item.HasOneTimeUsage);
                    Console.WriteLine("==================================");
                }
                Console.WriteLine("> Points restants : " + LeftPoints);
                Console.WriteLine();
            } else
            {
                Console.WriteLine();
                Console.WriteLine(" Aucun �quipement trouv� pour " + Name);
                Console.WriteLine(" D�pensez vos " + LeftPoints + " points dans la boutique !");
                Console.WriteLine();
            }
        }

        public Item GetThisItemFromEquipment(int item)
        {
            var thisItem = Equipment[item];
            return thisItem;
        }

        public Item GetThisItemFromEquipment(string item)
        {
            var thisItem = Equipment.FirstOrDefault(x => x.Name == item);
            return thisItem;
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public int MaxPoints
        {
            get
            {
                return maxPoints;
            }
        }

        public int LeftPoints
        {
            get
            {
                return leftPoints;
            }
        }

        public int Priority
        {
            get
            {
                return priority;
            }

            set
            {
                priority = value;
            }
        }
    }
}