﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LpdwGladiator
{
    class Program
    {

        static void Main(string[] args)
        {

            /* 1 - BOUTIQUE */
            Shop shop = new Shop();
            
            // Aucun item dans la boutique
            //Console.WriteLine("Shop 1 :");
            //shop.GetItems();

            // Ajout des items par défaut dans la boutique
            //shop.SetItems();
            Console.WriteLine("##### 1 - Test Shop init avec tous les Items #####");
            shop.GetItems();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            // Affichage des charactéristiques d'un item
            Console.WriteLine("##### 1 - Test Shop Recherche Par Nom #####");
            shop.ShowThisItemFromList("Dague");
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();
            Console.WriteLine("##### 1 - Test Shop Recherche Par Numéro #####");
            shop.ShowThisItemFromList(3);
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();
            Console.WriteLine("##### Test Shop Recherche Item Inconnu #####");
            shop.ShowThisItemFromList("Minigun");
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();



            /* 2 - GLADIATEURS */
            Gladiator gladiateur_1 = new Gladiator("Gladiateur 1");
            Gladiator gladiateur_2 = new Gladiator("Gladiateur 2");
            Gladiator gladiateur_3 = new Gladiator("Gladiateur 3");

            // Aucun équipement pour g1
            Console.WriteLine("##### 2 - Test Gladiator 1 Pas D'équipement #####");
            gladiateur_1.ShowEquipement();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            Console.WriteLine("##### 2 - Test Gladiator 1 ajout d'une dague #####");
            gladiateur_1.ChooseNewItem("Dague");
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();
            Console.WriteLine("##### 2 - Test Gladiator 1 ajout d'un casque #####");
            gladiateur_1.ChooseNewItem("Casque");
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();
            Console.WriteLine("##### 2 - Test Gladiator 1 ajout par nombre #####");
            gladiateur_1.ChooseNewItem(4);
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            Console.WriteLine("##### 2 - Test Gladiator 1 Voir la liste des équipements #####");
            gladiateur_1.ShowEquipement();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            // Aucun équipement pour g2
            Console.WriteLine("##### 3 - Test Gladiator 2 Pas D'équipement #####");
            gladiateur_2.ShowEquipement();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            Console.WriteLine("##### 3 - Test Gladiator 2 Dague #####");
            gladiateur_2.ChooseNewItem("Dague");
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();
            Console.WriteLine("##### 3 - Test Gladiator 2 Épée #####");
            gladiateur_2.ChooseNewItem("Épée");
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();
            Console.WriteLine("##### 3 - Test Gladiator 2 ajout par nombre #####");
            gladiateur_1.ChooseNewItem(4);
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            Console.WriteLine("##### 2 - Test Gladiator 2 Voir la liste des équipements #####");
            gladiateur_2.ShowEquipement();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            // G3
            Console.WriteLine("##### 4 - Test Gladiator 3 Dague #####");
            gladiateur_3.ChooseNewItem("Trident");
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();
            Console.WriteLine("##### 4 - Test Gladiator 3 Épée #####");
            gladiateur_3.ChooseNewItem("Dague");
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            Console.WriteLine("##### 4 - Test Gladiator 3 Voir la liste des équipements #####");
            gladiateur_3.ShowEquipement();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();


            /* Teams */
            Team team1 = new Team("Team 1", "La Team Numbeur Ouane");
            Team team2 = new Team("Team 2", "La Team Numbeur Tou");

            team1.AddGladiator(gladiateur_1);
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            team1.AddGladiator(gladiateur_2);
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            team1.ShowTeamGladiators();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            team1.SetGladiatorsPriority(gladiateur_1, gladiateur_2);
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            team1.ShowTeamGladiators();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();
        

            team2.AddGladiator(gladiateur_3);
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            team2.SetGladiatorsPriority(gladiateur_3);
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            team2.ShowTeamGladiators();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            /* Players */
            Player player1 = new Player("NOM", "Prénom", "Pseudo");
            Player player2 = new Player("NAME", "First Name", "Nickname");

            player1.AddTeam(team1);
            player2.AddTeam(team2);

            player1.ShowPlayerTeams();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();
            player2.ShowPlayerTeams();
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            /* DUEL */
            // Gladiator gladiator1 = team1.TeamGladiatorsSorted[1]; //FIXME
            // Gladiator gladiator2 = team2.TeamGladiatorsSorted[1];
            //Duel duel1 = new Duel(gladiator1, gladiator2);
            Duel duel1 = new Duel(gladiateur_1, gladiateur_2); //Manual gladiator input for debug
            Console.WriteLine("< Entrée pour continuer >");
            Console.Read();

            Console.Read();
        }

    }
}
