using System;
using System.Collections.Generic;
using System.Text;

namespace LpdwGladiator
{
    public class Item
    {
        private string name;
        private int cost;
        private double attack;
        private double parry;
        private int priority;
        private bool onetimeusage;

        public Item(string name, int cost, double attack, double parry, int priority, bool onetimeusage)
        {
            this.name = name;
            this.cost = cost;
            this.attack = attack;
            this.parry = parry;
            this.priority = priority;
            this.onetimeusage = onetimeusage;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Cost
        {
            get { return cost; }
            set { cost = value; }
        }

        public double Attack
        {
            get { return attack; }
            set { attack = value; }
        }

        public double Parry
        {
            get { return parry; }
            set { parry = value; }
        }

        public bool HasOneTimeUsage
        {
            get { return onetimeusage; }
            set { onetimeusage = value; }
        }

        public int Priority
        {
            get
            {
                return priority;
            }

            set
            {
                priority = value;
            }
        }
    }
}
