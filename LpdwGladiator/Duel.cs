using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LpdwGladiator
{
    public class Duel
    {
        private Gladiator gladiator1;
        private Gladiator gladiator2;
        private Item weaponG1;
        private Item weaponG2;

        public Duel(Gladiator gladiator1, Gladiator gladiator2)
        { 
            this.gladiator1 = gladiator1;
            this.gladiator2 = gladiator2;
            var gstarting = WhoWillStart(gladiator1, gladiator2);
            Console.WriteLine("Gladiateur " + gstarting + " attaque en premier !");
            Console.WriteLine("< Entr�e pour continuer >");
            Console.Read();
            var fight1 = new Fight(gstarting, weaponG1, weaponG2, gladiator1, gladiator2);
        }

        public int WhoWillStart(Gladiator gladiator1, Gladiator gladiator2)
        {
            List<Item> Equipementg1 = gladiator1.GetEquipment();
            List<Item> Equipementg2 = gladiator2.GetEquipment();

            //phase 1 : priorit� des armes
            List<Item> Equipementg1Sorted = Equipementg1.OrderByDescending(o => o.Priority).ToList();
            List<Item> Equipementg2Sorted = Equipementg2.OrderByDescending(o => o.Priority).ToList();


            int i = 0;

            var FirstG1Item = Equipementg1Sorted[i];
            var FirstG2Item = Equipementg2Sorted[i];
            var imaxG1 = Equipementg1Sorted.Count();
            var imaxG2 = Equipementg2Sorted.Count();

            while (FirstG1Item.Priority == FirstG2Item.Priority) 
            {   
                Console.WriteLine("G1 : " + FirstG1Item.Name + " = G2 : " + FirstG2Item.Name);
                i++; //Compare with next items
                if (i < imaxG1) //Prevent out of range error
                {
                    FirstG1Item = Equipementg1Sorted[i];
                }
                if (i < imaxG2) //Prevent out of range error
                {
                    FirstG2Item = Equipementg2Sorted[i];
                }

                if (i == 5) //Protect from infinite loop
                {
                    Dice diceOfTwo = new Dice(1, 2); //Random between 1 and 2
                    int randomplayer = diceOfTwo.DiceRoll();
                    return randomplayer;
                }
            }

            if (FirstG1Item.Priority > FirstG2Item.Priority)
            {
                Console.WriteLine("G1 : " + FirstG1Item.Name + " > G2 :" + FirstG2Item.Name);
                weaponG1 = FirstG1Item;
                weaponG2 = FirstG2Item;
                return 1;
            } else {
                Console.WriteLine("G1 : " + FirstG1Item.Name + " < G2 :" + FirstG2Item.Name);
                weaponG1 = FirstG1Item;
                weaponG2 = FirstG2Item;
                return 2;
            }

        }

        public Gladiator Gladiator1
        {
            get
            {
                return gladiator1;
            }

            set
            {
                gladiator1 = value;
            }
        }

        public Gladiator Gladiator2
        {
            get
            {
                return gladiator2;
            }

            set
            {
                gladiator2 = value;
            }
        }
    }
}
